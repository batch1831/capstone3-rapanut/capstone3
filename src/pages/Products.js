import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

import ProductCard from "../components/ProductCard";

export default function Products() {
  //State tha will be used to store the courses retrieved form the database
  const [products, setProducts] = useState([]);

  // To be use for validating the "role" of the user.
  const { user } = useContext(UserContext);

  //Retrieve the courses from the database upon initial render of the Course component.
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return user.isAdmin ? (
    <Navigate to="/admin" />
  ) : (
    <>
      <h1>Available Products</h1>
      {products}
    </>
  );
}

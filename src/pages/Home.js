import Banner from "../components/Banner";

export default function Home() {
  const data = {
    title: "RaPee",
    content: "Leading E-commerce App",
    destination: "/products",
    label: "View Products",
  };

  return (
    <>
      <Banner data={data} />
    </>
  );
}

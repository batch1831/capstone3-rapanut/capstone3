// import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, Button } from "react-bootstrap";

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, stocks } = productProp;

  return (
    <Card className="p-3 my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description: </Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price: </Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Text>Stocks: {stocks}</Card.Text>
        {/*We will be able to select a specific course through its url*/}
        <Button as={Link} to={`/products/${_id}`} variant="primary">
          Details
        </Button>

        {/*<Button variant="primary mx-1" onClick={enroll}>Enroll</Button>
		        <Button variant="danger mx-1" onClick={unEnroll}>Unenroll</Button>*/}
      </Card.Body>
    </Card>
  );
}
